import {
  HTTP_ACCOUNT_FETCHING,
  HTTP_ACCOUNT_SUCCESS,
  HTTP_ACCOUNT_FAILED,
  apiUrl,
  server,
  YES,
  config
} from "../constants";
import Axios from "axios";
import { NotificationManager } from "react-notifications";

export const setStateToAccountFetching = () => ({
  type: HTTP_ACCOUNT_FETCHING
});

export const setStateToAccountSuccess = payload => ({
  type: HTTP_ACCOUNT_SUCCESS,
  payload
});

export const setStateToAccountFailed = () => ({
  type: HTTP_ACCOUNT_FAILED
});


export const autoLogin =(history)=>{
  return () =>{
    if(localStorage.getItem(server.LOGIN_PASSED) == YES){
      setTimeout(()=> history.push("/home"))
    }
  }
}

export const login = (history,credential)=>{
  return async (dispatch , getState) => {
    dispatch(setStateToAccountFetching());
    
    try {
      let result = await Axios.post(apiUrl+`/`+server.LOGIN_URL,JSON.stringify(credential),config);
      
      if(result.data !== ""){
        NotificationManager.success("Login Success", "", 3000);
        localStorage.setItem(server.LOGIN_PASSED,YES);
        localStorage.setItem(server.NAME_ADMIN,result.data.firstname);
        let name = result.data.firstname +' '+ result.data.surname;
        localStorage.setItem(server.NAME_ADMIN,name);
        getState().appReducer.app.forceUpdate();

        dispatch(setStateToAccountSuccess(result.data.fristname));
        history.push("/home");
      }
      else
      {
        NotificationManager.error("Username or Password Incorrect", "", 3000);
        dispatch(setStateToAccountFailed());
      }

    } catch (error) {
      NotificationManager.error(`${error}`, "", 3000);
      dispatch(setStateToAccountFailed());
    }
  }
}

export const register = (history, credential) => {
  return async dispatch => {
    dispatch(setStateToAccountFetching());

    try {
     
      let result = await Axios.post(
        apiUrl + `/` + server.REGISTER_URL,
        JSON.stringify(credential),
        config
      );
      
      if (result.data === "success") {
        NotificationManager.success("Register Success", "", 3000);
        dispatch(setStateToAccountSuccess({ res: result }));
        history.goBack();
      } 
      else if(result.data === "Repeatedname"){
        NotificationManager.error("Repeated name", "", 3000);
        dispatch(setStateToAccountFailed());
      }
      else {
        NotificationManager.error("Register Unsuccess", "", 3000);
        dispatch(setStateToAccountFailed());
      }
    } catch (error) {
      NotificationManager.error(`${error}`, "", 3000);
      dispatch(setStateToAccountFailed());
    }
  };
};
