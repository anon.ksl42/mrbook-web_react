import {
  HTTP_PRODUCT_FETCHING,
  HTTP_PRODUCT_SUCCESS,
  HTTP_PRODUCT_FAILED,
  server,
  apiUrl,
  config
} from "../constants";
import Axios from "axios";
import { NotificationManager } from "react-notifications";

export const setStateToProductFetching = () => ({
  type: HTTP_PRODUCT_FETCHING
});

export const setStateToProductSuccess = payload => ({
  type: HTTP_PRODUCT_SUCCESS,
  payload
});

export const setStateToProductFailed = () => ({
  type: HTTP_PRODUCT_FAILED
});

export const getProduct = pageId => {
  return dispatch => {
    setStateToProductFetching();
    doGetProduct(dispatch, pageId);
  };
};
export const addBook = (pageId, formdata,history) => {
  return async dispatch => {
    dispatch(setStateToProductFetching());
   let res = await Axios.post(
      apiUrl + `/` + server.ADDBOOK,
      formdata
    );
    if(res.data === "Successbook"){
      NotificationManager.success("AddBook Success", "", 3000);
      history.push("/home");
      
      
    }
    else{
      NotificationManager.error("AddBook UnSuccess", "", 3000);
      history.push("/home");
      dispatch(setStateToProductFailed());
    }
    await doGetProduct(dispatch, pageId);
  };
};

export const deleteProduct = (pageId, id) => {
  return async dispatch => {
    dispatch(setStateToProductFetching());
    await Axios.post(
      apiUrl + `/` + server.DELETE_URL,
      JSON.stringify(id),
      config
    );
    await doGetProduct(dispatch, pageId);
  };
};

const doGetProduct = async (dispatch, pageId) => {
  try {
    let result = await Axios.post(
      apiUrl + `/` + server.PRODUCT_URL,
      JSON.stringify(pageId),
      config
    );
    //console.log(result.data.count,result.data.listBook);
    localStorage.setItem(server.COUNT_PAGE, result.data.count);
    dispatch(setStateToProductSuccess(result.data.listBook));
  } catch (error) {
    console.log(error);
    dispatch(setStateToProductFailed());
  }
};
