import React, { Component } from "react";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import Nav from "./components/header/header";
import Menu from "./components/menu/menu";
import Register from "./components/register/register";
import Login from "./components/login/login";
import Home from "./components/home/home";
import Footer from "./components/footer/footer";
import ProductCreate from "./components/productCreate/productCreate";
import { server, YES } from "./constants";
import { setApp } from "./actions/app.action";
import { connect } from "react-redux";

const isLogin = () => {
  return localStorage.getItem(server.LOGIN_PASSED) === YES;
};

const SecuredRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLogin() === true ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);

class App extends Component {
  componentDidMount() {
    this.props.setApp(this);
  }

  redirectToLogin = () => {
    return <Redirect to="/login" />;
  };

  render() {
    return (
      <Router>
        <div>
          
          {isLogin() && <Nav />}
          {isLogin() && <Menu />}
          <NotificationContainer />
          <Switch>
           
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <SecuredRoute path="/home" component={Home} />
            <SecuredRoute path="/product-create" component={ProductCreate} />
            <SecuredRoute
              exact={true}
              path="/"
              component={this.redirectToLogin}
            />
            <SecuredRoute path="*" component={this.redirectToLogin} />
          </Switch>
          {isLogin() && <Footer />}
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  setApp
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
