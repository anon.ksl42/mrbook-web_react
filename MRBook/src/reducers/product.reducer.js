import { HTTP_PRODUCT_FETCHING, HTTP_PRODUCT_SUCCESS, HTTP_PRODUCT_FAILED } from "../constants";

const initialState = {
  result: null,
  isFetching: false,
  isError: false
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case HTTP_PRODUCT_FETCHING:
      return { ...state, isFetching: true, result: null, isError: false };

    case HTTP_PRODUCT_SUCCESS:
      return { ...state, isFetching: false, result: payload, isError: false };

    case HTTP_PRODUCT_FAILED:
      return { ...state, isFetching: false, result: null, isError: true };

    default:
      return state;
  }
};
