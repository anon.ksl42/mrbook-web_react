import { combineReducers } from "redux";
import accountReducer from './account.reducer'
import appReducer from './app.reducer'
import productReducer from './product.reducer'

export default combineReducers({
    accountReducer,
    appReducer,
    productReducer

})