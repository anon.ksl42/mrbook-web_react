import React, { Component } from "react";
import { server } from "../../constants";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { NotificationManager } from "react-notifications";
import swal from "sweetalert";

// import * as action from '../../actions/account.action'

class Header extends Component {
  render() {
    return (
      <nav className="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        {/* Left navbar links */}
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" data-widget="pushmenu" href="#">
              <i className="fa fa-bars" />
            </a>
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={() => {
                swal({
                  title: "Are you sure to sign out?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true
                }).then(willDelete => {
                  if (willDelete) {
                    this.props.history.push("/login");
                    localStorage.removeItem(server.NAME_ADMIN);
                    localStorage.removeItem(server.LOGIN_PASSED);
                    NotificationManager.success("Logout Success", "", 3000);
                    this.props.appReducer.app.forceUpdate();
                  }
                });
              }}
            >
              <i className="fa fa-sign-out" /> Logout
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}
const mapStateToProps = ({ appReducer }) => ({
  appReducer
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
