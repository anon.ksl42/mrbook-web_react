import React, { Component } from "react";
import { Formik } from "formik";
import "./productCreate.css";
class ProductCreate extends Component {
  showForm = () => {
    //return ()
  };

  render() {
    return (
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">
                  <i className="fa fa-plus"></i> AddBook{" "}
                </h1>
              </div>
              {/* /.col */}
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  {/* <li className="breadcrumb-item">
                    <a href="#">Home</a>
                  </li> */}
                  <li className="breadcrumb-item active">AddBook</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        {/* Main content */}
        <section className="content" style={{ maxWidth: "80%" }}>
          <Formik>{props => this.showForm(props)}</Formik>
        </section>
        <div className="container-fluid">
          <div className="card">
           
            <div className="card-body">
              
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCreate;
