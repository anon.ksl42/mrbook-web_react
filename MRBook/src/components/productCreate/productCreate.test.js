import React from 'react';
import { shallow } from 'enzyme';
import ProductCreate from './productCreate';

describe('<ProductCreate />', () => {
  test('renders', () => {
    const wrapper = shallow(<ProductCreate />);
    expect(wrapper).toMatchSnapshot();
  });
});
