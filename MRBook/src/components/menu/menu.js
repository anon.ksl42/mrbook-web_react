import React, { Component } from "react";
import { server } from "../../constants";
import { Link } from "react-router-dom";

class Menu extends Component {
  showName = () => {
    return (
      <a href="#" className="d-block">
        {localStorage.getItem(server.NAME_ADMIN)}
      </a>
    );
  };

  render() {
    return (
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        <Link to="/home" className="brand-link">
          <img
            src="dist/img/AdminLTELogo.png"
            alt="AdminLTE Logo"
            className="brand-image img-circle elevation-3 "
            style={{ opacity: ".8" }}
          />
          <span className="brand-text font-weight-light">MR.BOOK</span>
        </Link>
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src="dist/img/ROPF2fQ.png"
                className="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div className="info">{this.showName()}</div>
          </div>
          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <li className="nav-item">
                <Link to="/home" className="nav-link">
                  <i className="mdi mdi-book-open-page-variant mdi-24px"/>
                  {" "}
                  <p>Bookshelf</p>
                </Link>
              </li>
              {/* <li className="nav-item has-treeview menu-open">
                <a href="#" className="nav-link ">
                  <i className="nav-icon fa fa-dashboard" />
                  <p>
                    Starter Pages
                    <i className="right fa fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <a href="#" className="nav-link ">
                      <i className="fa fa-circle-o nav-icon" />
                      <p>Active Page</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link">
                      <i className="fa fa-circle-o nav-icon" />
                      <p>Inactive Page</p>
                    </a>
                  </li>
                </ul>
              </li> */}
            </ul>
          </nav>
        </div>
      </aside>
    );
  }
}

export default Menu;
