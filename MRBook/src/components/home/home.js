import React, { Component } from "react";
import { connect } from "react-redux";
import NumberFormat from "react-number-format";
import * as action from "./../../actions/product.action";
import { server, config } from "../../constants";
// import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import swal from "sweetalert";
import { Formik } from "formik";
import Axios from "axios";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      limit: 4
    };
  }

  componentDidMount() {
    this.handlePageChange(this.state.page);
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber });
    this.props.getProduct(pageNumber);
  }
  listPageNumber = () => {
    let pagelength = localStorage.getItem(server.COUNT_PAGE);
    return (
      <Pagination
        itemClass="page-item"
        linkClass="page-link"
        activePage={this.state.page}
        itemsCountPerPage={4}
        totalItemsCount={parseInt(pagelength)}
        pageRangeDisplayed={5}
        onChange={this.handlePageChange.bind(this)}
      />
    );
  };

  tableRows = () => {
    const { result } = this.props.productReducer;

    if (result != null) {
      return result.map((i, id) => (
        <tr key={id}>
          <td style={{ width: "4%", textAlign: "center" }}>
            <span style={{ marginRight: 10, minHe: 100 }}>
              <img
                src={`${i.path_ImageCover}`}
                className="img-thumbnail"
                style={{ maxWidth: 150 }}
              />
            </span>
          </td>
          <td style={{ verticalAlign: "middle" }}>{i.productName}</td>
          <td style={{ verticalAlign: "middle" }}>
            <NumberFormat
              value={i.productPrice}
              displayType={"text"}
              thousandSeparator={true}
              decimalScale={2}
              fixedDecimalScale={true}
              prefix={"฿"}
            />
          </td>
          <td style={{ textAlign: "center", verticalAlign: "middle" }}>
            <button type="button" className="btn btn-success">
              <i className="fa fa-eye" />
            </button>{" "}
            <button type="button" className="btn btn-info">
              <i className="nav-icon fa fa-edit" />
            </button>{" "}
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => {
                swal({
                  title: "Are you sure to delete?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true
                }).then(willDelete => {
                  if (willDelete) {
                    this.props.deleteProduct(this.state.page, i.productId);
                  }
                });
              }}
            >
              <i className="fa fa-trash" />
            </button>
          </td>
        </tr>
      ));
    }
  };
  showPreviewImage = values => {
    if (values.file_obj) {
      return <img src={values.file_obj} style={{ height: 100 }} />;
    }
  };
  showFormBook = ({
    values,
    errors,
    handleChange,
    handleSubmit,
    setFieldValue,
    handleReset,
    touched
  }) => {
    return (
      <div className="modal-dialog modal-dialog-centered " role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Add Book</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <form onSubmit={handleSubmit} encType="multipart/form-data">
            <div className="modal-body">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="mdi mdi-pencil-box-multiple mdi-18px" />
                  </span>
                </div>
                <input
                  name="ProductName"
                  value={values.ProductName}
                  onChange={handleChange}
                  maxLength={10}
                  type="text"
                  className="form-control"
                  placeholder="ชื่อหนังสือ"
                />
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="mdi mdi-cash-usd-outline mdi-18px" />
                  </span>
                </div>
                <input
                  name="ProductPrice"
                  value={values.ProductPrice}
                  onChange={handleChange}
                  maxLength={10}
                  type="text"
                  className="form-control"
                  placeholder="ราคา"
                />
              </div>

              <div className="form-group">
                <label>Image-Cover</label>
                <input
                  type="file"
                  onChange={e => {
                    e.preventDefault();
                    setFieldValue("file", e.target.files[0]); // for upload
                    setFieldValue(
                      "file_obj",
                      URL.createObjectURL(e.target.files[0])
                    ); // for preview image
                  }}
                  className="form-control-file border"
                />
              </div>
              <div align="center">
              <div className="form-group">{this.showPreviewImage(values)}</div>
              </div>
              
              <div className="form-group">
                <label>File-Ebook</label>
                <input
                  type="file"
                  onChange={e => {
                    e.preventDefault();
                    setFieldValue("ebook", e.target.files[0]); // for upload
                  }}
                  className="form-control-file border"
                />
              </div>
              
              <div className="form-group">
                <select
                  className="form-control select2 select2-hidden-accessible"
                  name="CateID"
                  value={values.CateID}
                  onChange={handleChange}
                  style={{ width: "100%" }}
                >
                  <option value={0}>- เลือกประเภทหนังสือ -</option>
                  <option value={1}>แฟนตาซี</option>
                  <option value={2}>ผจญภัย</option>
                  <option value={3}>สยองขวัญ</option>
                </select>
              </div>
              <div className="form-group">
                <textarea
                  className="form-control"
                  name="ProductDetail"
                  value={values.ProductDetail}
                  onChange={handleChange}
                  placeholder="รายละเอียด หนังสือ ..."
                />
              </div>
            </div>
            <div className="modal-footer">
              <button type="submit" className="btn btn-primary">
                Save
              </button>
              <button
                type="reset"
                className="btn btn-secondary"
                data-dismiss="modal"
                onClick={handleReset}
              >
                Close
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">
                  <i className="mdi mdi-bookmark-multiple"></i> Bookshelf{" "}
                </h1>
              </div>
              {/* /.col */}
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  {/* <li className="breadcrumb-item">
                    <a href="#">Home</a>
                  </li> */}
                  <li className="breadcrumb-item active"> Bookshelf</li>
                </ol>
              </div>
            </div>
          </div>
        </div>

        <section className="content">
          <div className="container-fluid">
            <div className="card">
              <div className="card-header">
                <h3 className="card-title">
                  {" "}
                  <label>
                    <input
                      type="search"
                      className="form-control form-control-sm"
                      placeholder="search"
                    />
                  </label>
                </h3>
                <div className="card-tools">
                  {/* <Link
                    to="/product-create"
                    style={{ float: "right", margin: 0, width: 100 }}
                    className="btn btn-outline-success"
                  >
                    Add book
                  </Link> */}
                  <button
                    type="button"
                    className="btn btn-info"
                    data-toggle="modal"
                    data-target="#exampleModalCenter"
                  >
                    Add Book
                  </button>
                </div>
              </div>

              <div className="card-body">
                <table
                  className="table  table-hover"
                  style={{ height: 100, maxHeight: 300 }}
                >
                  <tbody>
                    <tr>
                      <th style={{ width: "9%", textAlign: "left" }}>Image</th>
                      <th style={{ width: "50%" }}>Title</th>
                      <th style={{ width: "9%" }}>Price</th>
                      <th style={{ width: "20%", textAlign: "center" }}>
                        Action
                      </th>
                    </tr>
                    {this.tableRows()}
                  </tbody>
                </table>
              </div>
              <div className="card-footer clearfix">
                {this.listPageNumber()}
              </div>
            </div>
          </div>
        </section>

        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModalCenter"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
          data-backdrop="static"
        >
          <Formik
            initialValues={{
              ProductName: "",
              ProductDetail: "",
              ProductPrice: "",
              CateID: ""
            }}
            //validate={values => this.validateForm(values)}
            onSubmit={async (values, actions) => {
              let formData = new FormData();
              formData.append("ProductName", values.ProductName);
              formData.append("ProductDetail", values.ProductDetail);
              formData.append("ProductPrice", parseInt(values.ProductPrice));
              formData.append("CateID", parseInt(values.CateID));
              formData.append("files", values.file);
              formData.append("ebook", values.ebook);
              
              await this.props.addBook(this.state.page,formData,this.props.history);

              actions.setSubmitting(false);
            }}
          >
            {props => this.showFormBook(props)}
          </Formik>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ productReducer }) => ({
  productReducer
});

const mapDispatchToProps = {
  ...action
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
