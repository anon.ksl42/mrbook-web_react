import React, { Component } from "react";
import {connect} from 'react-redux'
import {login,autoLogin} from '../../actions/account.action'
import { Formik } from "formik";


class Login extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       username:"",
       password:""
    }
  }
  componentDidMount(){
    this.props.autoLogin(this.props.history);
  }
  render() {
    return (
      <div className="card login-box">
        <div className="login-logo">
          <a href="#">
            <b>Admin</b>MRB
          </a>
        </div>
        <div className="card-body">
          <form method="post">
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                
                <span className="input-group-text">
                  <i className="fa fa-user" />
                </span>
              </div>
              <input
                onChange={e=>this.setState({username:e.target.value})}
                type="text"
                name="Username"
                className="form-control"
                placeholder="Username"
              />
            </div>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="fa fa-lock" />
                </span>
              </div>
              <input
                type="password"
                onChange={e=>this.setState({password:e.target.value})}
                name="Password"
                className="form-control"
                placeholder="Password"
              />
            </div>
            <div className="form-group">
              <button 
                type="button" 
                className="btn btn-block btn-info"
                onClick={()=>{
                  this.props.login(this.props.history,this.state)
                }}
              >
                Sign In
              </button>
              <button 
                type="button" 
                className="btn btn-block"
                onClick={()=>{
                  this.props.history.push("/register")
                }}
              >
                Register
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({accountReducer}) => ({
  accountReducer
})

const mapDispatchToProps = {
  login,autoLogin
}


export default connect(mapStateToProps,mapDispatchToProps)(Login);
