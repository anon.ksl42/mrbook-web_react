import React, { Component } from "react";
import { connect } from "react-redux";
import { register } from "../../actions/account.action";
import { Formik } from "formik";
class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Username: "",
      Password: "",
      Firstname: "",
      Surname: ""
    };
  }

  showForm = (
    { values, errors, handleChange, handleSubmit, handleBlur, touched },
    { isError }
  ) => {
    return (
      <form
        onSubmit={handleSubmit}
        onKeyDown={e => {
          if (e.key === 13) {
            handleSubmit();
          }
        }}
      >
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-pencil" />
            </span>
          </div>
          <input
            name="Username"
            maxLength={10}
            value={values.Username}
            onChange={handleChange}
            type="text"
            //className={`form-control ${errors.IsValid1 ? "is-invalid" : `${isError === true ? "is-invalid":""}`}`}
            className={`form-control ${errors.IsValid1 ? "is-invalid" : ""}`}
            placeholder="Username"
          />
          {/* {isError && (<div className="invalid-feedback">ชื่อผู้ใช้ซํ้า</div>)} */}
          {errors.Username && (
            <div className="invalid-feedback">{errors.Username}</div>
          )}
        </div>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-pencil" />
            </span>
          </div>
          <input
            name="Password"
            value={values.Password}
            onChange={handleChange}
            type="password"
            className={`form-control ${errors.IsValid2 ? "is-invalid" : ""}`}
            placeholder="Password"
          />
          {errors.Password && (
            <div className="invalid-feedback">{errors.Password}</div>
          )}
        </div>

        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-pencil" />
            </span>
          </div>
          <input
            name="ComfirmPassword"
            value={values.ComfirmPassword}
            onChange={handleChange}
            type="password"
            className={`form-control ${errors.IsValid3 ? "is-invalid" : ""}`}
            placeholder="ComfirmPassword"
          />
          {errors.ComfirmPassword && (
            <div className="invalid-feedback">{errors.ComfirmPassword}</div>
          )}
        </div>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-pencil"></i>
            </span>
          </div>
          <input
            name="Firstname"
            value={values.Firstname}
            onChange={handleChange}
            type="text"
            className={`form-control ${errors.IsValid4 ? "is-invalid" : ""}`}
            placeholder="Firstname"
          />
          {errors.Firstname && (
            <div className="invalid-feedback">{errors.Firstname}</div>
          )}
        </div>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="fa fa-pencil" />
            </span>
          </div>
          <input
            name="Surname"
            value={values.Surname}
            onChange={handleChange}
            type="text"
            className={`form-control ${errors.IsValid5 ? "is-invalid" : ""}`}
            placeholder="Lastname"
          />
          {errors.Surname  && (
            <div className="invalid-feedback">{errors.Surname}</div>
          )}
        </div>
        <div className="form-group">
          <button
            type="submit"
            className="btn btn-block btn-info"
            // onClick={() => this.props.register(this.props.history, this.state)}
          >
            Register
          </button>
          <button
            type="button"
            className="btn btn-block"
            onClick={e => {
              e.preventDefault();
              this.props.history.goBack();
            }}
          >
            Cancel
          </button>
        </div>
      </form>
    );
  };

  isNullOrEmpty = s => {
    return s === "" || s === null || s === undefined;
  };

  validateForm = values => {
    const errors = {};
    let validateP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,16}$/;
    if (this.isNullOrEmpty(values.Username)) {
      errors.Username = "กรุณาระบุ Username";
      errors.IsValid1 = "is-invalid";
    } else if (values.Username.length < 5) {
      errors.Username = "Username ควรมี 5-10 ตัวอักษร";
      errors.IsValid1 = "is-invalid";
    } else if (this.isNullOrEmpty(values.Password)) {
      errors.Password = "กรุณาระบุ Password";
      errors.IsValid2 = "is-invalid";
    } else if (
      !validateP.test(values.Password) ||
      values.Password === values.Username
    ) {
      errors.Password =
        "กรุณาเลือกใช้รหัสผ่านที่มีความยาว 8-16 ตัวอักษร ซึ่งรหัสผ่านนั้นต้องแตกต่างกับ Username ของคุณ และรหัสผ่านของคุณจะต้องมีทั้งตัวหนังสือพิมพ์เล็กและพิมพ์ใหญ่ และมีตัวเลข (0-9) อย่างน้อย 1 ตัว";
      errors.IsValid2 = "is-invalid";
    } else if (values.ComfirmPassword !== values.Password) {
      errors.ComfirmPassword = "รหัสผ่านของคุณไม่ตรงกัน, กรุณาใส่อีกครั้ง";
      errors.IsValid3 = "is-invalid";
    } else if (this.isNullOrEmpty(values.Firstname)) {
      errors.Firstname = "กรุณาระบุ Firstname";
      errors.IsValid4 = "is-invalid";
    } else if (this.isNullOrEmpty(values.Surname)) {
      errors.Surname = "กรุณาระบุ Surname";
      errors.IsValid5 = "is-invalid";
    }
    return errors;
  };

  render() {
    return (
      <div className="card login-box">
        <div className="login-logo">
          <a href="#">
            <b>Register</b>
          </a>
        </div>
        <div className="card-body">
          <Formik
            initialValues={{
              Username: "",
              Password: "",
              ComfirmPassword: "",
              Firstname: "",
              Surname: ""
            }}
            validate={values => this.validateForm(values)}
            onSubmit={(values, actions) => {
              this.props.register(this.props.history, values);
              actions.setSubmitting(false);
            }}
          >
            {props => this.showForm(props, this.props.accountReducer)}
          </Formik>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ accountReducer }) => ({ accountReducer });

const mapDispatchToProps = {
  register
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
