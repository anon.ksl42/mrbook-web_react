// Account Page
export const HTTP_ACCOUNT_FETCHING = "HTTP_ACCOUNT_FETCHING";
export const HTTP_ACCOUNT_SUCCESS = "HTTP_ACCOUNT_SUCCESS";
export const HTTP_ACCOUNT_FAILED = "HTTP_ACCOUNT_FAILED";

// Product Page
export const HTTP_PRODUCT_FETCHING = "HTTP_PRODUCT_FETCHING";
export const HTTP_PRODUCT_SUCCESS = "HTTP_PRODUCT_SUCCESS";
export const HTTP_PRODUCT_FAILED = "HTTP_PRODUCT_FAILED";

export const apiUrl = "http://localhost:61944/api/v1";
export const imageUrl = "http://localhost:61944";

export const YES = "YES";
export const APP_INIT = "APP_INIT";

export const config = {
  headers: {
    "Content-Type": "application/json;;charset=UTF-8"
  }
};
// export const configImage = {
//   headers: {
//     accept: "application/json",
//     "Content-Type": "multipart/form-data;charset=UTF-8"
//   }
// };

export const server = {
  LOGIN_URL: `account/login`,
  REGISTER_URL: `account/register`,
  PRODUCT_URL: `product/book`,
  DELETE_URL: `product/delete`,
  ADDBOOK: `product/InsertBook`,
  LOGIN_PASSED: `YES`,
  NAME_ADMIN: "ADMIN",
  COUNT_PAGE: "COUNT"
};
